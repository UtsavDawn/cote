﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace COTEModel
{
    [Table("Candidate")]
    [DataContract]
    public class EfCandidate
    {
        [Key]
        [DataMember]
        public int CandidateId { get; set; }
        [DataMember]
        public string LoginEmailId { get; set; }
        [DataMember]
        public string ContactNumber { get; set; }
        [DataMember]
        public byte Experience { get; set; }
    }
}
