﻿using System.Linq;
using COTEData;
using COTERepository.Interface;

namespace COTERepository.Implements
{
    public class CoteRepository<T>: ICoteRepository<T> where T: class
    {
        public CoteRepository()
        {
        }

        private readonly CoteContext _context;
        public CoteRepository(CoteContext context)
        {
            _context = context;
        }

        protected CoteContext Context
        {
            get { return _context; }
        }
        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        public IQueryable<T> GetAllInclude(string path)
        {
            return Context.Set<T>().Include(path);
        }

        public T Get(object id)
        {

            return Context.Set<T>().Find(id);
        }

        public void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }
    }
}
