﻿using System.Linq;

namespace COTERepository.Interface
{
    public interface ICoteRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAllInclude(string path);
        T Get(object id);
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        void SaveChanges();
    }
}
