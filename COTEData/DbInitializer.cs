﻿using System.Data.Entity;

namespace COTEData
{
    public class DbInitializer
    {
        public static void InitializeDatabase()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CoteContext, Configuration>());
        }
    }
}
