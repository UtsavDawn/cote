﻿using System.Data.Entity.Migrations;

namespace COTEData
{
    internal sealed class Configuration : DbMigrationsConfiguration<CoteContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CoteContext context)
        {

        }
    }
}