namespace COTEData
{
    using System.Data.Entity.Migrations;
    
    public partial class CreateCandidateTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidate",
                c => new
                    {
                        CandidateId = c.Int(nullable: false, identity: true),
                        LoginEmailId = c.String(nullable: false),
                })
                .PrimaryKey(t => t.CandidateId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Candidate");
        }
    }
}
