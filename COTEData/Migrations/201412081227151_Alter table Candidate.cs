namespace COTEData
{
    using System.Data.Entity.Migrations;
    
    public partial class AltertableCandidate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidate", "Experience", c => c.Byte(nullable: false));
            AddColumn("dbo.Candidate", "ContactNumber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AddColumn("dbo.Candidate", "Experience", c => c.Byte(nullable: false));
            AddColumn("dbo.Candidate", "ContactNumber", c => c.String(nullable: false));
        }
    }
}
