﻿using COTEService.Implements;
using COTEService.Interface;
using StructureMap;

namespace COTEWcfService.ServiceImplementation
{
    public class BaseService
    {
        public IEfCandidateService CandidateService { get; set; }

        public BaseService()
        {
            CandidateService = ObjectFactory.GetInstance<EfCandidateService>();
        }
    }
}
