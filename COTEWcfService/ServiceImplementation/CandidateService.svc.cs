﻿using COTEModel;
using COTEWcfService.ServiceInterface;

namespace COTEWcfService.ServiceImplementation
{
    public class CandidateService : BaseService, ICandidateService
    {
        public bool IsNewUser(string loginEmailId)
        {
            return CandidateService.GetCandidateByLoginEmailId(loginEmailId) == null;
        }

        public string AddNewCandidate(EfCandidate candidate)
        {
            if (!IsNewUser(candidate.LoginEmailId)) return "Failure";
            CandidateService.AddCandidate(candidate);
            return "Success";
        }
    }
}