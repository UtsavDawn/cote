﻿using System.ServiceModel;
using System.ServiceModel.Web;
using COTEModel;

namespace COTEWcfService.ServiceInterface
{
    [ServiceContract]
    public interface ICandidateService
    {
        [WebGet(
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/IsNewUser/{loginEmailId}")]
        [OperationContract]
        bool IsNewUser(string loginEmailId);

        [WebInvoke(
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/AddNewCandidate",
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        [OperationContract]
        string AddNewCandidate(EfCandidate candidate);
    }
}
