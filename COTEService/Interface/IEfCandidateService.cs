﻿using COTEModel;

namespace COTEService.Interface
{
    public interface IEfCandidateService
    {
        EfCandidate GetCandidateByLoginEmailId(string loginEmailId);
        void AddCandidate(EfCandidate candidate);
    }
}
