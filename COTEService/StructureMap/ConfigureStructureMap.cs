﻿using COTEData;
using COTEModel;
using COTERepository.Implements;
using COTERepository.Interface;
using COTEService.Implements;
using COTEService.Interface;
using StructureMap;
using StructureMap.Configuration.DSL;

namespace COTEService.StructureMap
{
    public class ConfigureStructureMap
    {
        private static bool _hasStarted;

        public void BootstrapStructureMap()
        {
            ObjectFactory.Initialize(x =>
            {
                x.IncludeRegistry(new RepositoryRegistry());
                x.IncludeRegistry(new ServiceRegistry());
            });
            
        }

        public static void Restart()
        {

            if (_hasStarted)
                ObjectFactory.ResetDefaults();
            else
            {
                DbInitializer.InitializeDatabase();
                Bootstrap();
                _hasStarted = true;
            }
        }

        public static void Bootstrap()
        {
            new ConfigureStructureMap().BootstrapStructureMap();
        }

    }

    internal class RepositoryRegistry : Registry
    {
        internal RepositoryRegistry()
        {
            For<ICoteRepository<EfCandidate>>().Use<CoteRepository<EfCandidate>>();
        }
    }


    internal class ServiceRegistry : Registry
    {
        internal ServiceRegistry()
        {
            For<IEfCandidateService>().Use<EfCandidateService>();
        }
    }
}

