﻿using System;
using COTEData;
using COTEModel;
using COTERepository.Interface;
using StructureMap;

namespace COTEService.Implements
{
    public class EfBaseService: IDisposable
    {
        private readonly CoteContext context;

        public void Dispose()
        {
            context.Dispose();
        }
        public ICoteRepository<EfCandidate> CandidateRepository { get; private set; }

        public EfBaseService()
        {
            context = new CoteContext();
            CandidateRepository = ObjectFactory.With("context").EqualTo(context).GetInstance<ICoteRepository<EfCandidate>>();
        }
    }
}
