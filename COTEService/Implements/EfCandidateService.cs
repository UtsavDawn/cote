﻿using System.Linq;
using COTEModel;
using COTEService.Interface;

namespace COTEService.Implements
{
    public class EfCandidateService : EfBaseService, IEfCandidateService
    {
        public EfCandidate GetCandidateByLoginEmailId(string loginEmailId)
        {
            return CandidateRepository.GetAll().SingleOrDefault(l => l.LoginEmailId.Equals(loginEmailId));
        }

        public void AddCandidate(EfCandidate candidate)
        {
            CandidateRepository.Add(candidate);
            CandidateRepository.SaveChanges();
        }
    }
}
